#!/usr/bin/env zsh

notify() {
   echo "###########################"
   echo " ${2} $1"
   echo "###########################"
}

install_update() {
   target=$1
   if [ ! -e ~/.config/vcsh/repo.d/${target}.git/HEAD ]; then
      notify ${target} install
      vcsh clone ${BBURL}/tmux.git
   else
      notify ${target} update
   fi
   vcsh ${target} pull origin master
}


BBURL="git@bitbucket.org:stefanlendl"
if [[ '--https' == "$1" ]]; then
echo "https"
	BBURL="https://stefanlendl@bitbucket.org/stefanlendl"
else
   # certificate for bitbucket - or setup pw
   # check - abort oterhwise!!!
   echo "checking git:bitbucket.org connection..."
   ssh -o ConnectTimeout=5 -T git@bitbucket.org
   if [ "0" != "$?" ]; then
      echo "no access to git@bitbucket => install ssh keys or register in the bitbucket account"
      echo -n 'continue with https connection? (Y/n):' && read -qs key
      # read -rp $'continue with https connection? (Y/n):' -n1 key
      if [ "n" == "$key" ]; then
         exit
      else
         BBURL="https://stefanlendl@bitbucket.org/stefanlendl"
      fi
   fi
fi

# check zsh, vim, git, vcsh, tmux, ag, 
REQ=('zsh' 'vim' 'git' 'vcsh')
echo -e "following commands are required: $REQ\nchecking..."
for r in $REQ; do
   command -v $r &>/dev/null || (echo "command $r not found"; exit)
done

install_update zsh
. ~/.zshrc ~/.zprofile

if [ ! -d ~/.zprezto/.git ]; then
   notify prezto install
   git clone --recursive https://github.com/sorin-ionescu/prezto.git "${ZDOTDIR:-$HOME}/.zprezto"
   setopt EXTENDED_GLOB
   for rcfile in "${ZDOTDIR:-$HOME}"/.zprezto/runcoms/^README.md(.N); do
      ln -s "$rcfile" "${ZDOTDIR:-$HOME}/.${rcfile:t}"
   done
else
   notify prezto update
   (cd ~/.zprezto;
   git pull && git submodule update --init --recursive )
fi

install_update tmux
echo "run <prefix>I to install tpm packages"

install_update vim
~/.vim/init.sh

install_update git
